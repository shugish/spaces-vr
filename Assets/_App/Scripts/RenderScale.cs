﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class RenderScale : MonoBehaviour {

    public float renderScale = 2.0f; // Render Scale

    // Use this for initialization
    void Start () {
        XRSettings.eyeTextureResolutionScale = renderScale; // setup render scale / eye texture resolution scale
    }

}
