﻿using HTC.UnityPlugin.Vive;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR;

namespace IGS_Platform
{
    public class IGS_SceneManager : MonoBehaviour
    {
        private bool canPress;
        private static int sceneGrade = 0;
        private static int currentSceneGrade = 0;
        private static int sceneQuestionNum = 0;
        private static int currentSceneQuestionNum = 0;
        //private static string sceneTag = "Init";
        private bool invokeLastSeneOnce = true;

        static string _set_scene_tag = "Init";
        public static string set_scene_tag
        {
            get
            {
                if (isPause)
                {
                    //Debug.Log("--- PAUSE ---");
                    return "--- PAUSE ---";
                }

                return _set_scene_tag;
            }

            set
            {
                _set_scene_tag = value;
            }
        }


        [Header("General Settings")]
        public float renderScale = 2.0f;        // Render Scale
        public int minSceneTimeInSec = 10;      // Seconds Overall
        public string startScene = "998_Begin"; // Starting Scene (after calibration scene)
        public string endScene = "999_End";     // Ending Scene 
        public TextMesh contentText;            // UI Text Object for Countdown, questions, fixation and more
        public GameObject controllerPointers = null;
        public Color blankColor = Color.gray;

        [Header("Questionnaire")]
        [SerializeField] private GameObject Questionnaire = null;
        [SerializeField] private GameObject Fixation = null;
        [SerializeField] private Button doneButton = null;
        [SerializeField] private List<Button> toggleButtons = new List<Button>();
        [SerializeField] private Color buttonSelectedColor;
        [SerializeField] private Color buttonClearColor;
        [SerializeField] private List<GameObject> Questions = new List<GameObject>();
        [SerializeField] private List<Text> Answers = new List<Text>();


        [Header("Audio Settings")]
        [SerializeField] private AudioSource m_Audio;       // Reference to the audio source that will play effects when the user looks at it and when it fills.
        [SerializeField] private AudioClip m_OnCountdownEnd;  // The clip to play when the countdown finishes.


        private const int WD_VAL = 2000;
        private const int COUNTDOWN_TIME = 5;
        private static float INIT_BLANK_SCREEN_TIME = 20.0f;

        private GameObject env;
        private bool doneAnswer = false;
        private int watchdog = WD_VAL;
        private bool doneInitBlankScreen = false;
        private bool setInitBlankScreen = true;
        private int timeLeft = 30; // for first screen (10s countdown + 20s blank)
        private List<int> scenes = new List<int>();


#if _IGS_HeatmapManager
        static bool _set_heatmap_camera = false;
        public static bool set_heatmap_camera
        {
            get { return _set_heatmap_camera; }
            set
            {
                _set_heatmap_camera = value;
            }
        }

        static bool _destroy_heatmap_camera = false;
        public static bool destroy_heatmap_camera
        {
            get { return _destroy_heatmap_camera; }
            set
            {
                _destroy_heatmap_camera = value;
            }
        }
#endif
        private void Awake()
        {
            canPress = false;
            Invoke("ResetPress", 1f);
            DynamicGI.UpdateEnvironment();
            XRSettings.eyeTextureResolutionScale = renderScale; // setup render scale / eye texture resolution scale
        }

        private bool IsForwardPressed()
        {
            // get trigger down         
            if (ViveInput.GetPressDownEx(HandRole.RightHand, ControllerButton.Trigger) ||
                ViveInput.GetPressDownEx(HandRole.LeftHand, ControllerButton.Trigger))
            {
                //Debug.Log("HandRole Trigger");
                return true;
            }

            return false;
        }

        private bool IsBackPressed()
        {
            return false;
        }

        private void ResetPress()
        {
            canPress = true;
        }

        private void Start()
        {
            SetRecordPath();

            scenes = new List<int>(Enumerable.Range(1, SceneManager.sceneCountInBuildSettings - 3)); // This creates a list with values from 1 to number of scenes minus 3 scenes:000_calibration, 999_End, 998_Start

            StartCoroutine("LoseTime");
            timeLeft = minSceneTimeInSec;
            Time.timeScale = 1; //Just making sure that the timeScale is right

            if (controllerPointers != null)
            {
                controllerPointers.SetActive(false);
            }

            if (Questionnaire != null)
            {
                Questionnaire.SetActive(false);
            }

            if (Fixation != null)
            {
                Fixation.SetActive(false);
            }

            DisableDoneButton();

            ClearToggleButtons();

            HideQuestions();

#if _IGS_HeatmapManager
            set_heatmap_camera = true; // Instantiate new HM camera
#endif
        }

        public static bool isPause = false;
        private void Update()
        {
            setContentText();

            // Update if Pause ('P' is pressed)
            if (Input.GetKeyUp (KeyCode.P))
            {
                isPause = !isPause;

                if (isPause)
                {   // Hide the current model when pause starts
                    env = GameObject.Find("Environment");
                    if (env != null)
                    {
                        env.SetActive(false);
                    }
                    contentText.text = "PAUSE";
                    Debug.Log("--- PAUSE START---");
                }
                else
                {   // Show the current model when pause ends
                    if (env != null)
                    {
                        env.SetActive(true);
                    }
                    contentText.text = ""; // Clear the contentText
                    Debug.Log("--- PAUSE END---");
                }
            }

            // Continue if trigger pressed and not before 10sec
            if (IsForwardPressed() && (timeLeft <= 0))
            {
                set_scene_tag = "Heatmap Capturing";
                Heatmap.capturing = true;
                WritePupilData();

                //Hide the current model before showing questionnaire
                env = GameObject.Find("Environment");
                if (env != null)
                {
                    env.SetActive(false);
                }

                if (scenes.Count == 0) // If no scenes left
                {
                    if (invokeLastSeneOnce == true)
                    {
                        Invoke("LastSequence", 0.0f); // Invoke last scene once..
                        invokeLastSeneOnce = false; 
                    }

                }
                else
                {
                    Invoke("StartSequence", 0.0f); // Invoke next scene
                }

                timeLeft = minSceneTimeInSec; // reset the scene time
            }

            // Quit application with Escape key
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                //Application.Quit();
                QuitGame();
            }
        }

        static bool firstTime = true;
        void setContentText()
        {
            //Check if the current Active Scene's name is first Scene
            if (SceneManager.GetActiveScene().name == startScene)
            {
                if (firstTime)
                {
                    if(timeLeft > 0)
                    {
                        contentText.text = "Starting in " + timeLeft + " seconds";
                    }
                    else
                    {
                        if(setInitBlankScreen)
                        {
                            Invoke("InitBlankScreen", 0);
                            setInitBlankScreen = false; // init only once
                        }

                        if (doneInitBlankScreen)
                        {   // when init blank screen is completed
                            contentText.text = "Press trigger to begin";
                            if (IsForwardPressed())
                            {
                                contentText.text = "";
                                firstTime = false;
                            }
                        }
                    }
                }
                else
                {
                    contentText.text = "";
                }
            }
            else
            {
                //contentText.text = "";
            }
        }

        void InitBlankScreen()
        {
            set_scene_tag = "Init blank screen";
            contentText.text = ""; // Clear the contentText
            StartCoroutine(BlankTheScreen());
        }

        IEnumerator BlankTheScreen()
        {
            //SteamVR_Fade.Start(blankColor, 0);     // Set start color
            yield return new WaitForSeconds(INIT_BLANK_SCREEN_TIME);
            //SteamVR_Fade.Start(Color.clear, 0);     // Clear color
            doneInitBlankScreen = true;
        }

        void StartSequence()
        {
            contentText.text = ""; // Clear the contentText
            StartCoroutine(NewTimeline());
        }

        void LastSequence()
        {
            StartCoroutine(LastTimeline());
        }

        private void ResetQuestionsVars()
        {
            // Reset vars
            doneAnswer = false;
            sceneGrade = 0;
            sceneQuestionNum = 0;
            watchdog = WD_VAL;
        }

        public void DoneQuestionnaire()
        {
            doneAnswer = true;
            sceneGrade = currentSceneGrade; // Save selected answer
            sceneQuestionNum = currentSceneQuestionNum;
        }

        public void EnableDoneButton()
        {
            if (doneButton != null)
            {
                doneButton.interactable = true;
            }
        }

        public void DisableDoneButton()
        {
            if (doneButton != null)
            {
                doneButton.interactable = false;
            }
        }

        private void ClearToggleButtons()
        {
            for (int i = 0; i < toggleButtons.Count; i++)
            {
                toggleButtons[i].image.color = buttonClearColor;
            }
        }

        private void SetAnswers(int index)
        {
            if (index == 0)
            { // Set answers for Q1
                Answers[0].text = "1";
                Answers[1].text = "2";
                Answers[2].text = "3";
                Answers[3].text = "4";
                Answers[4].text = "5";
                Answers[5].text = "6";
                Answers[6].text = "7";
                Answers[7].text = "Dislike"; // Text1-ext
                Answers[8].text = "Like"; // Text7-ext
            }
            else
            { // Set answers for Q2
                Answers[0].text = "Creative";
                Answers[1].text = "Sportive / Playful";
                Answers[2].text = "Work / Study";
                Answers[3].text = "Rest";
                Answers[4].text = "Punish";
                Answers[5].text = "Social";
                Answers[6].text = "Preform a task";
                Answers[7].text = ""; // Text1-ext
                Answers[8].text = ""; // Text7-ext
            }
        }

        private void HideQuestions()
        {
            for (int i = 0; i < Questions.Count; i++)
            {
                Questions[i].SetActive(false);
            }
        }

        private void ShowQuestion(int index)
        {
            HideQuestions();
            SetAnswers(index-1);
            Questions[index - 1].SetActive(true);
        }

        private void ShowQuestionnaire()
        {
            set_scene_tag = "Questionnaire";
            controllerPointers.SetActive(true); // Show controllers
            Questionnaire.SetActive(true); // Show Question
        }

        private void HideQuestionnaire()
        {
            controllerPointers.SetActive(false); // Hide controller
            Questionnaire.SetActive(false); // Hide Questionnaire
            DisableDoneButton(); // Disable "Done Button"
            ResetQuestionsVars();
            ClearToggleButtons(); // Clear all buttons selected color
        }

        IEnumerator NewTimeline()
        {
            /* 
             * Display Questionnaire 
             */
            currentSceneQuestionNum = 1; // First Q
            ShowQuestion(1);
            ShowQuestionnaire();

            /* 
             * Prepare next scene in background 
             */
            int randomIndex = Random.Range(0, scenes.Count); // Get a random index from the list of remaining level 
            int scneneNum = scenes[randomIndex];
            scenes.RemoveAt(randomIndex); // Removes the level from the list
            Debug.Log("Loading scene #" + scneneNum + ":" + NameFromIndex(scneneNum) + ". -> Scenes left: " + scenes.Count);
            StartCoroutine(LoadNextScene(scneneNum)); // Begin loading next scene in background

            /*
             * Wait for Questionnaire to complete
             */
            while (doneAnswer != true) // Wait for the answer
            {
                yield return new WaitForSeconds(0.1f);
                timeLeft = minSceneTimeInSec; // keep update it otherwise the update function will replace a scene
                watchdog--; //watchdog not to wait forever
            }

            /* 
             * Reset state: Hide Questionnaire, controller & Disable "Done Button"
             */
            HideQuestionnaire();


            /* 
             * Display Questionnaire 
             */
            currentSceneQuestionNum = 2; // Second Q
            ShowQuestion(2);
            ShowQuestionnaire();


            /*
             * Wait for Questionnaire to complete
             */
            while (doneAnswer != true) // Wait for the answer
            {
                yield return new WaitForSeconds(1);
                timeLeft = minSceneTimeInSec; // keep update it otherwise the update function will replace a scene
                watchdog--; //watchdog not to wait forever
            }

            /* 
             * Reset state: Hide Questionnaire, controller & Disable "Done Button"
             */
            HideQuestionnaire();

            /*
             * Show Fixation screen for 2 sec
             */
            set_scene_tag = "Fixation";
            Fixation.SetActive(true);
            yield return new WaitForSeconds(2.0f);
            Fixation.SetActive(false);

            /* 
             * Blank screen for 300ms
             */
            set_scene_tag = "Blank screen";
            //SteamVR_Fade.Start(blankColor, 0);     // Set start color
            yield return new WaitForSeconds(0.3f);
            //SteamVR_Fade.Start(Color.clear, 0);     // Clear color

            /* 
             * Play sound for next scene
             */
            m_Audio.clip = m_OnCountdownEnd;
            m_Audio.Play();

            // Activate next scene
            StartCoroutine (ActivateNextScene());
        }

        IEnumerator LastTimeline()
        {
            /* 
             * Display Questionnaire 
             */
            currentSceneQuestionNum = 1; // First Q
            ShowQuestion(1);
            ShowQuestionnaire();

            /*
             * Wait for Questionnaire to complete
             */
            while (doneAnswer != true) // Wait for the answer
            {
                yield return new WaitForSeconds(1);
                timeLeft = minSceneTimeInSec; // keep update it otherwise the update function will replace a scene
                watchdog--; //watchdog not to wait forever
            }

            /* 
             * Reset state: Hide Questionnaire, controller & Disable "Done Button"
             */
            HideQuestionnaire();

            /* 
             * Display Questionnaire 
             */
            currentSceneQuestionNum = 2; // Second Q
            ShowQuestion(2);
            ShowQuestionnaire();


            /*
             * Wait for Questionnaire to complete
             */
            while (doneAnswer != true) // Wait for the answer
            {
                yield return new WaitForSeconds(1);
                timeLeft = minSceneTimeInSec; // keep update it otherwise the update function will replace a scene
                watchdog--; //watchdog not to wait forever
            }

            /* 
             * Reset state: Hide Questionnaire, controller & Disable "Done Button"
             */
            HideQuestionnaire();

            /*
             * Load ending scene ("thank You" scene), wait for 5 sec and then quit application
             */
            SceneManager.LoadScene(endScene);// Load End scene
            yield return new WaitForSeconds(5.0f);
            //Application.Quit();
            QuitGame();
        }


        AsyncOperation async;
        IEnumerator LoadNextScene(int index)
        {
            // The Application loads the Scene in the background as the current Scene runs.
            async = SceneManager.LoadSceneAsync(index);
            //Dont activate the scene
            async.allowSceneActivation = false;
            yield return async;
        }

        IEnumerator ActivateNextScene()
        {
            //Activate the loaded scene
            async.allowSceneActivation = true;

            while (!async.isDone)
            {
                yield return null;
            }
            set_scene_tag = "ACTIVE SCENE";
        }

        void fadeScreen(float duration)
        {
            SteamVR_Fade.Start(Color.white, 0);     //set start color
            SteamVR_Fade.Start(Color.clear, duration);  //set and start fade to
        }

        //Simple Coroutine
        IEnumerator LoseTime()
        {
            while (true)
            {
                yield return new WaitForSeconds(1);
                timeLeft--;
            }
        }

        private static string NameFromIndex(int BuildIndex)
        {
            string path = SceneUtility.GetScenePathByBuildIndex(BuildIndex);
            int slash = path.LastIndexOf('/');
            string name = path.Substring(slash + 1);
            int dot = name.LastIndexOf('.');
            return name.Substring(0, dot);
        }

        public static int GetSceneGrade()
        {
            return sceneGrade;
        }

        public static int GetSceneQuestionNum()
        {
            return sceneQuestionNum;
        }

        /*public static string GetSceneTag()
        {
            return sceneTag;
        }*/

        public void handleToggleButton(int index)
        {
            ClearToggleButtons();   // Clear all buttons selected color
            toggleButtons[index - 1].image.color = buttonSelectedColor; // Set Curent selected button
            EnableDoneButton();     // Enable Done button only after a button is pressed
            currentSceneGrade = index; // Set grade
        }


        private string GetLastIncrement(string path)
        {
            string[] directories = Directory.GetDirectories(path);
            List<int> directoryIncrements = new List<int>();
            foreach (string directory in directories)
            {
                var folderNameValue = int.Parse(directory.Substring(directory.Length - 3));
                directoryIncrements.Add(folderNameValue);
            }
            int currentIncrement = Mathf.Max(directoryIncrements.ToArray());
            //			int newIncrement = currentIncrement + 1;
            return (currentIncrement + 1).ToString("000");
            //directoryIncrements.ToArray()
        }

        public static string RecordingPath;
        private void SetRecordPath()
        {
            RecordingPath = PupilSettings.Instance.recorder.GetRecordingPath();

            System.Threading.Thread.Sleep(200);//Waiting for Pupil Service to create the incremented folder

            RecordingPath += "/" + GetLastIncrement(RecordingPath);

            if (!Directory.Exists(RecordingPath))
                Directory.CreateDirectory(RecordingPath);
        }


        private void WritePupilData()
        {
            // Write pupil timestamps to a file
            PupilTools.SaveRecording(RecordingPath);
        }


        private void QuitGame()
        {
            Debug.Log("Quit");

            // save any game data here
#if UNITY_EDITOR
            // Application.Quit() does not work in the editor so
            // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
            UnityEditor.EditorApplication.isPlaying = false;
#else
        CancelInvoke();
        StopAllCoroutines();
        Application.Quit();
#endif
        }
        /*
                private void Update()
                {
                    int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
                    int nextSceneIndex = currentSceneIndex;

                    if (IsForwardPressed() || Input.GetKeyUp(KeyCode.Space))
                    {
                        nextSceneIndex++;
                        if (nextSceneIndex >= SceneManager.sceneCountInBuildSettings)
                            //nextSceneIndex = 0; 
                            nextSceneIndex--; // IGS - stay on last scene (Finish scene) 
                    }
                    else if (IsBackPressed() || Input.GetKeyUp(KeyCode.Backspace))
                    {
                        nextSceneIndex--;
                        if (nextSceneIndex < 0)
                            nextSceneIndex = SceneManager.sceneCountInBuildSettings - 1;
                    }

                    if (nextSceneIndex == currentSceneIndex)
                    {
                        return;
                    }

                    Heatmap.capturing = true;
                    SceneManager.LoadScene(nextSceneIndex);
                }
        */
    }
}

