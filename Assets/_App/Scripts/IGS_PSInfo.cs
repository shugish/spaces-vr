﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IGS_PSInfo : MonoBehaviour {

    private ParticleSystem ps;

    // Use this for initialization
    void Start ()
    {
        ps = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update ()
    {
        ParticleSystem.Particle[] particles = new ParticleSystem.Particle[ps.particleCount];
        int num = ps.GetParticles(particles);
        Debug.Log(num + " Particles Found");
    }
}
