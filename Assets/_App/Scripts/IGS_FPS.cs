﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IGS_Platform
{
    public class IGS_FPS : MonoBehaviour
    {

        private int frameCount = 0;
        private double dt = 0.0;
        private double fps = 0.0;
        private double updateRate = 1.0;  // 4 updates per sec.

        private void Update()
        {
            frameCount++;
            dt += Time.deltaTime;
            if (dt > 1.0 / updateRate)
            {
                fps = frameCount / dt;
                frameCount = 0;
                dt -= 1.0 / updateRate;
                Debug.Log("** FPS " + (int)fps);
            }
        }
    }
}
