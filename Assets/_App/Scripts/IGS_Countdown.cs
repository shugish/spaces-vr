﻿using System.Collections;
using UnityEngine;

namespace IGS_Platform
{
    public class IGS_Countdown : MonoBehaviour
    {

        public int timeLeft = 10; //Seconds Overall
        public TextMesh countdown; //UI Text Object

        // Use this for initialization
        void Start()
        {
            StartCoroutine("LoseTime");
            Time.timeScale = 1; //Just making sure that the timeScale is right
        }

        // Update is called once per frame
        void Update()
        {
            if (timeLeft <= 0)
            {
                countdown.text = "Press the trigger to begin";
            }
            else
            {
                countdown.text = ("Starting in " + timeLeft + " seconds"); //Showing the Score on the Canvas
            }
        }

        //Simple Coroutine
        IEnumerator LoseTime()
        {
            while (true)
            {
                yield return new WaitForSeconds(1);
                timeLeft--;
            }
        }
    }
}