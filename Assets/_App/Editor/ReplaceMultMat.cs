﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

public class ReplaceMultMat : EditorWindow
{
    // Array to store an Object array of the scenes
    public Object[] scenes;
    public Material newMaterial;
    public string objToReplace = "Object_3";


    // Lists and string array for easier management
    List<string> sceneList = new List<string>();
    private int sceneIndex = 0;
    private string[] scenePath;

    // Editor text
    string replaceButton = "Replace";
    string status = "Idle...";
    System.DateTime timeStamp;

    // Menu entry
    [MenuItem("Tools/Replace Materials")]
    public static void ShowWindow()
    {
        EditorWindow window = GetWindow(typeof(ReplaceMultMat), false, "Replace Materials");
        window.autoRepaintOnSceneChange = true;
    }

    // Refresh the editor text when in focus
    void OnFocus()
    {
        status = "Idle...";
        // if (!Lightmapping.isRunning)
        if(!isReplaceDone)
        {
             replaceButton = "Replace";
        }
    }

    void OnGUI()
    {
        // "target" can be any class derrived from ScriptableObject 
        // (could be EditorWindow, MonoBehaviour, etc)
        ScriptableObject target = this;
        SerializedObject so = new SerializedObject(target);
        SerializedProperty scenesProperty = so.FindProperty("scenes");

        ScriptableObject target1 = this;
        SerializedObject so1 = new SerializedObject(target1);
        SerializedProperty matProperty = so1.FindProperty("newMaterial");
        EditorGUILayout.PropertyField(matProperty, true); // True means show children
        so1.ApplyModifiedProperties(); // Remember to apply modified properties

        ScriptableObject target2 = this;
        SerializedObject so2 = new SerializedObject(target2);
        SerializedProperty goObj = so2.FindProperty("objToReplace");
        EditorGUILayout.PropertyField(goObj, true); // True means show children
        so2.ApplyModifiedProperties(); // Remember to apply modified properties



        EditorGUILayout.PropertyField(scenesProperty, true); // True means show children
        so.ApplyModifiedProperties(); // Remember to apply modified properties

        if (GUILayout.Button(replaceButton)) // Button to start bake process
        {
            InitializeReplace();
        }
        EditorGUILayout.LabelField("Status: ", status);
        so.Update();
        so1.Update();
        so2.Update();
    }

    // If not replacing, set delegates, set scenes, and start replacing.
    // Otherwise, stop replacing and update editor text
    private bool isReplaceDone = false;
    void InitializeReplace()
    {
        SetScenes();
        while (!isReplaceDone)
        {
            ReplaceInNewScene();
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
        }
        //else
        {
            //Lightmapping.Cancel();
            UpdateReplaceProgress();
        }
    }

    // Create a string array of scenes to replace materials in
    private bool SetScenes()
    {
        // Reset values
        sceneList.Clear();
        sceneIndex = 0;

        // Get paths for scenes and store in list
        if (scenes.Length == 0)
        {
            status = "No scenes found";
            replaceButton = "Replace";
            return false;
        }
        else
        {
            for (int i = 0; i < scenes.Length; i++)
            {
                sceneList.Add(AssetDatabase.GetAssetPath(scenes[i]));
            }

            // Sort and put scene paths in array
            scenePath = sceneList.ToArray();
            return true;
        }
    }

    // Loop through scenes to bake and update on progress
    private void ReplaceInNewScene()
    {
        if (sceneIndex < scenes.Length)
        {
            EditorSceneManager.OpenScene(scenePath[sceneIndex]);
            timeStamp = System.DateTime.Now;
            ReplaceMat();
            UpdateReplaceProgress();
            sceneIndex++;
        }
        else
        {
            isReplaceDone = true;
            DoneReplacing("done");
        }
    }

    public GameObject go;
    private void ReplaceMat()
    {
        //go = GameObject.Find("Object_3");
        go = GameObject.Find(objToReplace); 

        MeshRenderer my_renderer = go.GetComponent <MeshRenderer>();
        if (my_renderer != null)
        {
            my_renderer.material = newMaterial;
        }

    }

    // Updates replacing progress
    private void UpdateReplaceProgress()
    {
        if (!isReplaceDone)
        {
            status = "Replacing " + (sceneIndex + 1).ToString() + " of " + scenes.Length.ToString();
            replaceButton = "Cancel";
        }
        else
        {
        //    DoneReplacing("cancel");
        }
        /*else if (!Lightmapping.isRunning)
        {
            DoneReplacing("cancel");
        }*/
    }

    // Saves the scene at the end of each bake before starting new bake
    private void SaveScene()
    {
        System.TimeSpan replaceSpan = System.DateTime.Now - timeStamp;
        string bakeTime = string.Format("{0:D2}:{1:D2}:{2:D2}",
            replaceSpan.Hours, replaceSpan.Minutes, replaceSpan.Seconds);
        Debug.Log("(" + sceneIndex.ToString() + "/" +
            scenes.Length.ToString() + ") " + "Done baking: " +
            EditorSceneManager.GetActiveScene().name + " after " + bakeTime +
            " on " + System.DateTime.Now.ToString());
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
    }

    // When done replacing, update the editor text
    private void DoneReplacing(string reason)
    {
        //Lightmapping.completed = null;
        sceneList.Clear();
        sceneIndex = 0;

        if (reason == "done")
        {
            status = "Replace is done";
            replaceButton = "Replace";
        }
        else if (reason == "cancel")
        {
            status = "Canceled";
            replaceButton = "Replace";
        }
    }
}
#endif