﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IGS_Platform;


namespace IGS_Platform
{
    public class IGS_HeatmapManager : MonoBehaviour
    {

        public GameObject InstantiateHeatmapPrefab;
        public Transform InstantiateParent;
        private GameObject instantiatedObj;
#if _IGS_HeatmapManager

        private void ActivateCam()
        {
            instantiatedObj.SetActive(true);
        }

        void Update()
        {
            if (IGS_SceneManager.set_heatmap_camera)
            {
                if (instantiatedObj == null) // instantiate if there is no heatmap camera
                {
                    instantiatedObj = (GameObject)Instantiate(InstantiateHeatmapPrefab, transform.position, transform.rotation);
                    instantiatedObj.transform.SetParent(InstantiateParent);
                    Invoke("ActivateCam", 0.0f); 
                }
            }

            if (IGS_SceneManager.destroy_heatmap_camera)
            {
                if (instantiatedObj != null) // destroy if there is a heatmap camera
                {
                    Destroy(instantiatedObj);
		            GameObject HeatmapRenderingCamera = GameObject.Find("RenderingCamera");
                    if (HeatmapRenderingCamera != null)
		            {
                        Destroy(HeatmapRenderingCamera);   
                    }
                    IGS_SceneManager.destroy_heatmap_camera = false;
                }
            }
        }
#endif
    }
}